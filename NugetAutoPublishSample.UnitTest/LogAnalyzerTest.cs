using System;
using System.Runtime.CompilerServices;
using Xunit;

namespace NugetAutoPublishSample.UnitTest
{
    public class LogAnalyzerTest
    {
        private LogAnalyzer logAnalyzer;

        public LogAnalyzerTest()
        {
            logAnalyzer = new LogAnalyzer();
        }

        [Fact]
        public void IsValidLogFileName_ValidFile_True()
        {
            bool result = logAnalyzer.IsValidLogFileName("aaaaa.SLF");

            Assert.True(result);
        }
    }
}
